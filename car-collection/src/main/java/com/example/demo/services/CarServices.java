package com.example.demo.services;

import com.example.demo.entities.Car;
import com.example.demo.entities.Collection;

import java.util.List;

public interface CarServices {
    void createCar(Car car);
    void deleteCar(int id);

    List<Car> getAll(String make, String model, String yearOfManufacture);

}
