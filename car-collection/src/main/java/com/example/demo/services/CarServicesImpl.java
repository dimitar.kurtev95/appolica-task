package com.example.demo.services;

import com.example.demo.entities.Car;
import com.example.demo.exceptions.DuplicatedEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServicesImpl implements CarServices {

    private CarRepository carRepository;

    @Autowired
    public CarServicesImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public void createCar(Car car) {
        try {
            carRepository.createCar(car);
        } catch (DuplicatedEntityException e) {
            throw new DuplicatedEntityException("Car");
        }
    }

    @Override
    public void deleteCar(int id) {
        try {
            carRepository.deleteCar(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<Car> getAll(String make, String model, String yearOfManufacture) {
        return carRepository.getAll(make,model,yearOfManufacture);
    }
}
