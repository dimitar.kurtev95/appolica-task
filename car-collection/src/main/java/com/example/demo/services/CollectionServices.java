package com.example.demo.services;

import com.example.demo.entities.Collection;

public interface CollectionServices {

    void createCollection(Collection collection);
    void deleteCollection(int id);
    void addCar(int carId, int collectionId);
    void deleteCar(int carId, int collectionId);

    long getCountByMake(int collectionId, String make);
    long getCountByModel(int collectionId, String model);
    long getCountByEngine(int collectionId, String engine);

}
