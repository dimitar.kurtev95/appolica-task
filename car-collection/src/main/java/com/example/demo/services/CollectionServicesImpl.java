package com.example.demo.services;

import com.example.demo.entities.Collection;
import com.example.demo.exceptions.DuplicatedEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.repositories.CollectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CollectionServicesImpl implements CollectionServices {

    private CollectionRepository collectionRepository;

    @Autowired
    public CollectionServicesImpl(CollectionRepository collectionRepository) {
        this.collectionRepository = collectionRepository;
    }


    @Override
    public void createCollection(Collection collection) {
        try {
            collectionRepository.createCollection(collection);
        } catch (DuplicatedEntityException e) {
            throw new DuplicatedEntityException(e.getMessage());
        }
    }

    @Override
    public void deleteCollection(int id) {
        try {
            collectionRepository.deleteCollection(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public void addCar(int carId, int collectionId) {
        try {
            collectionRepository.addCar(carId, collectionId);
        } catch (DuplicatedEntityException e) {
            throw new DuplicatedEntityException(e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }

    }

    @Override
    public void deleteCar(int carId, int collectionId) {
        try {
            collectionRepository.deleteCar(carId, collectionId);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public long getCountByMake(int collectionId, String make) {
        try {
            return collectionRepository.getCountByMake(collectionId, make);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public long getCountByModel(int collectionId, String model) {
        try {
            return collectionRepository.getCountByModel(collectionId, model);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public long getCountByEngine(int collectionId, String engine) {
        try {
            return collectionRepository.getCountByEngine(collectionId, engine);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }


}
