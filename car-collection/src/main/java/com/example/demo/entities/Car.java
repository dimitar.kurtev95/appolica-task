package com.example.demo.entities;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "cars")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //Hibernate relies on an auto-incremented database column to generate the primary key,
    @Column(name = "car_id")
    @EqualsAndHashCode.Include
    private int id;
    @Column(name = "make")
    private String make;
    @Column(name = "model")
    private String model;
    @Column(name = "year_manufacturing")
    private int yearOfManufacture;
    @Column(name = "color")
    private String color;
    @Column(name = "engine_displacement")
    private double engineDisplacement;
    @Column(name = "number_of_cylinders")
    private int numberOfCylinders;

}
