package com.example.demo.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String itemType, String attribute, String value){
        super(String.format("%s with %s %s not found",itemType,attribute,value));
    }

    public EntityNotFoundException(String msg){
        super(msg);
    }

}
