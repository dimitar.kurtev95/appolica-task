package com.example.demo.exceptions;

public class DuplicatedEntityException extends RuntimeException {

    public DuplicatedEntityException(String itemType, String attribute, String value) {
        super(String.format("%s with %s \"%s\" already exist in your collection", itemType, attribute, value));
    }

    public DuplicatedEntityException(String itemType) {
        super(String.format("This %s  already exist", itemType));
    }



    }
