package com.example.demo.controllers.rest;

import com.example.demo.entities.Car;
import com.example.demo.entities.Collection;
import com.example.demo.exceptions.DuplicatedEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.services.CollectionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/collections")
public class CollectionControllers {

    private CollectionServices collectionServices;

    @Autowired
    public CollectionControllers(CollectionServices collectionServices) {
        this.collectionServices = collectionServices;
    }

    @PostMapping
    public void createCollection(@RequestBody Collection collection) {
        try {
            collectionServices.createCollection(collection);
        } catch (DuplicatedEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping
    public void deleteCollection(int idCollection) {
        try {
            collectionServices.deleteCollection(idCollection);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/addCar/{collectionId}/{carId}")
    public void addCar(@PathVariable int collectionId, @PathVariable int carId) {
        try {
            collectionServices.addCar(collectionId, carId);
        } catch (EntityNotFoundException  e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (DuplicatedEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/deleteCar/{collectionId}/{carId}")
    public void deleteCar(@PathVariable int collectionId, @PathVariable int carId) {
        try {
            collectionServices.deleteCar(carId, collectionId);
        } catch (EntityNotFoundException  e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (DuplicatedEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("countByMake/{collectionId}")
    public long getCountByMake(@PathVariable int collectionId ,@RequestParam String make) {

        try {
            return collectionServices.getCountByMake(collectionId, make);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @GetMapping("countByModel/{collectionId}")
    public long getCountByModel(@PathVariable int collectionId, @RequestParam String model) {

        try {
            return collectionServices.getCountByMake(collectionId, model);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @GetMapping("countByEngine/{collectionId}")
    public long getCountByEngine(@PathVariable int collectionId, @RequestParam String engine) {

        try {
            return collectionServices.getCountByMake(collectionId, engine);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }


}
