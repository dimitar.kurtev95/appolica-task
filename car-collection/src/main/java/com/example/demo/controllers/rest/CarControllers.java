package com.example.demo.controllers.rest;

import com.example.demo.entities.Car;
import com.example.demo.exceptions.DuplicatedEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.services.CarServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/api/cars")
public class CarControllers {

    private CarServices carServices;

    @Autowired
    public CarControllers(CarServices carServices) {
        this.carServices = carServices;
    }

    @PostMapping
    public void createCar(@RequestBody Car car) {
        try {
            carServices.createCar(car);
        } catch (DuplicatedEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping
    public void deleteCar(int id) {
        try {
            carServices.deleteCar(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<Car> getAll(@RequestParam(value = "make", defaultValue = "") String make,
                            @RequestParam(value = "model", defaultValue = "") String model,
                            @RequestParam(value = "yearOfManufacture", defaultValue = "") String yearOfManufacture) {
        return carServices.getAll(make, model, yearOfManufacture);
    }
}
