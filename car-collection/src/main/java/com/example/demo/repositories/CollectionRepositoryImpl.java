package com.example.demo.repositories;

import com.example.demo.entities.Car;
import com.example.demo.entities.Collection;
import com.example.demo.exceptions.DuplicatedEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CollectionRepositoryImpl implements CollectionRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public CollectionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createCollection(Collection collection) {
        try (Session session = sessionFactory.openSession()) {
            session.save(collection);

        }
    }

    @Override
    public void deleteCollection(int id) {
        try (Session session = sessionFactory.openSession()) {
            Collection collectionToDelete = session.get(Collection.class, id);
            if (collectionToDelete == null) {
                throw new EntityNotFoundException("Collection", "id", String.valueOf(id));
            } else {
                session.beginTransaction();
                session.delete(collectionToDelete);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public void addCar(int collectionId, int carId) {
        try (Session session = sessionFactory.openSession()) {
            Collection collection = session.get(Collection.class, collectionId);

            if (collection == null) {
                throw new EntityNotFoundException("Collection", "id", String.valueOf(collectionId));
            }

            Car car = session.get(Car.class, carId);
            if (car == null) {
                throw new EntityNotFoundException("Collection", "id", String.valueOf(carId));
            }

            session.beginTransaction();
            if (!collection.getCars().add(car)) {
                throw new DuplicatedEntityException("Car");
            }
            session.getTransaction().commit();

        }
    }

    @Override
    public void deleteCar(int collectionId, int carId) {
        try (Session session = sessionFactory.openSession()) {

            Collection collection = session.get(Collection.class, collectionId);
            if (collection == null) {
                throw new EntityNotFoundException("Collection", "id", String.valueOf(collectionId));
            }

            Car car = session.get(Car.class, carId);
            if (car == null) {
                throw new EntityNotFoundException("Collection", "id", String.valueOf(carId));
            }

            session.beginTransaction();
            collection.getCars().remove(car);
            session.getTransaction().commit();
        }
    }

    @Override
    public long getCountByMake(int collectionId, String make) {
        long result = 0;

        try (Session session = sessionFactory.openSession()) {
            Collection collection = session.get(Collection.class, collectionId);
            if (collection == null) {
                throw new EntityNotFoundException("Collection", "id", String.valueOf(collectionId));
            }

            result = collection.getCars().stream()
                    .filter(car -> car.getMake().equals(make))
                    .count();
        }

        return result;
    }

    @Override
    public long getCountByModel(int collectionId, String model) {
        long result = 0;

        try (Session session = sessionFactory.openSession()) {
            Collection collection = session.get(Collection.class, collectionId);
            if (collection == null) {
                throw new EntityNotFoundException("Collection", "id", String.valueOf(collectionId));
            }

            result = collection.getCars().stream()
                    .filter(car -> car.getModel().equals(model))
                    .count();
        }

        return result;
    }

    @Override
    public long getCountByEngine(int collectionId, String engine) {
        long result = 0;

        try (Session session = sessionFactory.openSession()) {
            Collection collection = session.get(Collection.class, collectionId);
            if (collection == null) {
                throw new EntityNotFoundException("Collection", "id", String.valueOf(collectionId));
            }

            result = collection.getCars().stream()
                    .filter(car -> car.getEngineDisplacement()==Double.parseDouble(engine))
                    .count();
        }

        return result;
    }


}


