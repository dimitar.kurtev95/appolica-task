package com.example.demo.repositories;

import com.example.demo.entities.Car;
import com.example.demo.entities.Collection;

public interface CollectionRepository {

    void createCollection(Collection collection);

    void deleteCollection(int id);

    void addCar(int collectionId,int carId);

    void deleteCar(int collectionId, int carId);

    long getCountByMake(int collectionId, String make);
    long getCountByModel(int collectionId, String model);
    long getCountByEngine(int collectionId, String engine);

}
