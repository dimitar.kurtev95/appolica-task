package com.example.demo.repositories;

import com.example.demo.entities.Car;
import com.example.demo.exceptions.DuplicatedEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createCar(Car car) {
        try (Session session = sessionFactory.openSession()) {

            Query<Car> query = session
                    .createQuery("from Car where make LIKE :make" +
                            " AND model=:model" +
                            " AND yearOfManufacture=:yearOfManufacture " +
                            " AND color=:color" +
                            " AND engineDisplacement=:engineDisplacement" +
                            " AND numberOfCylinders=:numberOfCylinders", Car.class);
            query.setParameter("make", car.getMake());
            query.setParameter("model", car.getModel());
            query.setParameter("yearOfManufacture", car.getYearOfManufacture());
            query.setParameter("color", car.getColor());
            query.setParameter("engineDisplacement", car.getEngineDisplacement());
            query.setParameter("numberOfCylinders", car.getNumberOfCylinders());

            if (!query.list().isEmpty()) {
                throw new DuplicatedEntityException("Car");
            }
            session.save(car);
        }
    }

    @Override
    public void deleteCar(int id) {
        try (Session session = sessionFactory.openSession()) {
            Car carToDelete = session.get(Car.class, id);
            if (carToDelete == null) {
                throw new EntityNotFoundException("Car", "id", String.valueOf(id));
            } else {
                session.beginTransaction();
                session.delete(carToDelete);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public List<Car> getAll(String make, String model, String yearOfManufacture) {
        try (Session session = sessionFactory.openSession()) {

            StringBuilder queryString = new StringBuilder("from Car where make LIKE :make" +
                    " AND model LIKE :model");

            if (!yearOfManufacture.equals("")) {
                queryString.append(" AND yearOfManufacture=:yearOfManufacture ");
            }

            Query<Car> query = session.createQuery(queryString.toString(), Car.class);

            query.setParameter("make", "%" + make + "%");
            query.setParameter("model", "%" + model + "%");

            if (!yearOfManufacture.equals("")) {
                query.setParameter("yearOfManufacture", Integer.parseInt(yearOfManufacture));
            }

            return query.list();
        }
    }


}
