package com.example.demo.repositories;

import com.example.demo.entities.Car;

import java.util.List;

public interface CarRepository {

    void createCar(Car car);

    void deleteCar(int id);

    List<Car> getAll(String make, String model, String yearOfManufacture);


}
